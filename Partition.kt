// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> = 
    customers.filter{
        val (delOrder,undelOrder)=it.orders.partition{it.isDelivered}
        undelOrder.size > delOrder.size
    }.toSet()
